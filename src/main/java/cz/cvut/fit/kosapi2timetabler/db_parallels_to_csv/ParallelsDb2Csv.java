/*
 * Utilities to integrate KosAPI and Syllabus Plus
 * Generator of a CSV 
 * (c) 2018 FIT CTU (http://www.fit.cvut.cz)
 * Author: Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */

package cz.cvut.fit.kosapi2timetabler.db_parallels_to_csv;

import cz.cvut.fit.kosapi2timetabler.timetabler_db.Activity;
import cz.cvut.fit.kosapi2timetabler.timetabler_db.Module;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.DayOfWeek;
import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author guthondr
 */
public class ParallelsDb2Csv {
    public void exportAllActivities(final Path outputFile, final EntityManager entityManager, final int hoursPerDay) throws FileNotFoundException {
        exportActivitiesOfSelectedModules(outputFile, entityManager, hoursPerDay,null);
    }

    public void exportActivitiesOfSelectedModules(final Path outputFile,
                                                  final EntityManager entityManager,
                                                  final int hoursPerDay,
                                                  final Collection<String> modules) throws FileNotFoundException {
        try (final PrintWriter output = new PrintWriter(outputFile.toFile())) {
            final TypedQuery<Activity> q;
            if (modules == null) {
                q = entityManager.createQuery("SELECT a FROM Activity a",
                        Activity.class);
            } else {
                q = entityManager.createQuery("SELECT a FROM Activity a WHERE a.module.code IN :modules",
                        Activity.class);
                q.setParameter("modules", modules);
            }
            final Collection<Activity> activities = q.getResultList();

            for (final Activity a : activities) {
                if (a.getTimetableSlot() != null && a.getTimetableSlot().getDay() != null && a.getTimetableSlot().getFirstHour() != null) {
                    final DayOfWeek scheduledDay = a.getTimetableSlot().getDay();
                    final Byte firstHour = a.getTimetableSlot().getFirstHour();
                    output.printf("%d %d %d %d %d %d %d 0%n",
                            a.getModule().getDepartment() == null ? 0 : a.getModule().getDepartment().getKosId(),
                            a.getKosId(),
                            a.getTimetableSlot().getRoom() == null ? 0 : a.getTimetableSlot().getRoom().getKosId(),
                            a.getTeacher1() == null ? 0 : a.getTeacher1().getKosId(),
                            a.getTeacher2() == null ? 0 : a.getTeacher2().getKosId(),
                            scheduledDay == null ? 0 : scheduledDay.getValue(),
                            firstHour - (scheduledDay.getValue() - 1) * hoursPerDay + 1);
                }
            }
        }
    }
}
