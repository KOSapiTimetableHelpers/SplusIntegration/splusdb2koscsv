package cz.cvut.fit.kosapi2timetabler.db_parallels_to_csv;

import java.io.FileNotFoundException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class ParallelsDb2CsvMain {

    private static final String SETTINGS_FILENAME_DEFAULT = ".kosapi2db.properties";
    private static final String SETTINGS_HOURS_PER_DAY_PROPERTY = "cz.cvut.fit.kosapi2timetable.hours_per_day";
    private static final Path SETTINGS_FILE = Paths.get(
            System.getProperty("cz.cvut.fit.kosapi2timetable.parallels_to_csv.settings_file", Paths.get(System.getProperty("user.home"), SETTINGS_FILENAME_DEFAULT).toString()));
    private static final Path OUTPUT_FILE = Paths.get(
            System.getProperty("cz.cvut.fit.kosapi2timetable.parallels_to_csv.output_file", Paths.get(System.getProperty("user.home"), "kos_syllabus_export.csv").toString()));

    public static void main(String[] args) throws IOException {
        final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("TimetablerPG");
        final EntityManager ENTITY_MANAGER = ENTITY_MANAGER_FACTORY.createEntityManager();
        final Properties settings = new Properties();
        try {
            settings.load(Files.newInputStream(SETTINGS_FILE));
            new ParallelsDb2Csv().exportAllActivities(OUTPUT_FILE, ENTITY_MANAGER, Integer.valueOf(settings.getProperty(SETTINGS_HOURS_PER_DAY_PROPERTY)));
        } catch (NumberFormatException e) {
            throw new RuntimeException("Property " + SETTINGS_HOURS_PER_DAY_PROPERTY + " must be set to a valid integer in " + SETTINGS_FILE, e);
        } finally {
            ENTITY_MANAGER.close();
            ENTITY_MANAGER_FACTORY.close();
        }
    }
}
